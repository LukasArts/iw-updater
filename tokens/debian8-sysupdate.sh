#!/usr/bin/env bash
#
# Update system to current state
declare res

# IceWarp demands this explicitly during installation
dpkg --add-architecture i386
res=$?; echo $res

apt-get update
res=$?; echo $res

apt-get --just-print upgrade

# make sure that dependencies are satisfied
apt-get -yf install
res=$?; echo $res

apt-get -y upgrade
res=$?; echo $res

return $res
