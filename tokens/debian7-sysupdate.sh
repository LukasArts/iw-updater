#!/usr/bin/env bash
#
# Update system to current state
declare res

apt-get update
res=$?; echo $res

apt-get upgrade
res=$?; echo $res
