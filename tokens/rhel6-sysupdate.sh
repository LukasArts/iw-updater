#!/usr/bin/env bash
#
# Update system to current state
declare res

yum check-update
res=$?; echo $res

if [[ $res -eq 100 ]]
then
    yum -y update
    res=$?; echo $res
fi

return $res
