#!/usr/bin/env bash
#
# Sets time and enables time synchronization on RHEL 6
declare res

yum list installed ntp
res=$?; echo $res

if [[ $res -ne 0 ]]
then
    yum install -y ntp
    res=$?; echo $res
fi

chkconfig ntpd on
chkconfig --list ntpd

ntpd -q

service ntpd start

iptables -L -n | grep 'udp.*123'
res=$?

if [[ $res -ne 0 ]]
then
    lokkit --port=123:udp --update
fi

ntpq -p

ntpstat
res=$?

return $res
