#!/usr/bin/env bash
#
# Sets time and enables time synchronization on Ubuntu 14
declare res

apt-get isntall -y ntp
res=$?; echo $res

service ntp status
res=$?; echo $res

# On Ubuntu 14 this expects ntpd installed
timedatectl set-ntp yes
res=$?; echo $res

timedatectl
res=$?; echo $res

return $res
