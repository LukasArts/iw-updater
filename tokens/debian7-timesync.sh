#!/usr/bin/env bash
#
# Sets time and enables time synchronization on Debian 7
declare res

apt-install ntp
res=$?; echo $res

ntpq -p
res=$?; echo $res

return $res
