#!/usr/bin/env bash
#
# Sets time and enables time synchronization on Ubuntu 12
declare res

apt-get isntall -y ntp
res=$?; echo $res

service ntp status
res=$?; echo $res

return $res
