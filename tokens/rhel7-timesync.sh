#!/usr/bin/env bash
#
# Sets time and enables time synchronization on RHEL 7
declare res

# The command will fail if an NTP service is not installed
timedatectl set-ntp yes
res=$?; echo $res

if [[ $res -eq 0 ]]
then
    return 0 # time synchronization already configured
fi

yum list installed ntp
res=$?; echo $res

if [[ $res -eq 0 ]] # ntpd installed
then
    # TODO: handle ntpd settings
elif

yum install -y chrony
res=$?; echo $res

systemctl status chronyd
res=$?; echo $res

if [[ $res -eq 3 ]]
then
    systemctl start chronyd
    systemctl status chronyd
fi

timedatectl
