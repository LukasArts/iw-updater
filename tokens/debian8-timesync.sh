#!/usr/bin/env bash
#
# Sets time and enables time synchronization on RHEL 7
declare res


# Debian jessie relies on systemd build-in time synchronization by default
timedatectl set-ntp yes
res=$?; echo $res

# We need to wait a little otherwise systemctl returns 3
sleep 0.3

systemctl status systemd-timesyncd.service
res=$?; echo $res

return $res
