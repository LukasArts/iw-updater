Time synchronization
====================

systemd
-------
### RHEL7

potřebuje poskytovatele NTP synchronizace (např. chrony), jinak
    timedatectl set-ntp yes
    Failed to set ntp: NTP not supported.
`yum -y install chrony`

`chronyd -Q`

    2017-04-04T14:30:11Z chronyd version 2.1.1 starting (+CMDMON +NTP +REFCLOCK +RTC +PRIVDROP +DEBUG +ASYNCDNS +IPV6 +SECHASH)
    2017-04-04T14:30:11Z Generated key 1
    2017-04-04T14:30:16Z System clock wrong by 6308650.035268 seconds (ignored)
    2017-04-04T14:30:16Z chronyd exiting

`chronyd -q`

    2017-04-04T14:31:16Z chronyd version 2.1.1 starting (+CMDMON +NTP +REFCLOCK +RTC +PRIVDROP +DEBUG +ASYNCDNS +IPV6 +SECHASH)
    2017-04-04T14:31:16Z Frequency 0.000 +/- 1000000.000 ppm read from /var/lib/chrony/drift
    2017-04-04T14:31:20Z System clock wrong by 6308650.036669 seconds (step)
    2017-06-16T14:55:30Z chronyd exiting

`systemctl start chronyd`

`timedatectl set-ntp yes`

`timedatectl status`

                    ⋮
         NTP enabled: yes
    NTP synchronized: yes
                    ⋮

init
----
### RHEL6
`yum install ntp`
`chkconfig --list ntpd`

    ntpd           	0:off	1:off	2:on	3:on	4:on	5:on	6:off
    
`ntpstat`

    unsynchronised
      time server re-starting
       polling server every 64 s

povolit firewall
`lokkit --port=123:udp --update`

`iptables -L -n | grep 'udp.*123'`

    ACCEPT     udp  --  0.0.0.0/0            0.0.0.0/0           state NEW udp dpt:123

`ntpdate 0.cz.pool.ntp.org`
