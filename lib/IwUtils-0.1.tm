package require Expect

namespace eval ::iw:: {
    variable argsDict [dict create]
    variable exitCode
    variable config [dict create]
    namespace export *




    # Wrapper around exec function to provide more details for logging.
    if { [namespace which tcl_exec] eq {} } {
        rename ::exec tcl_exec
    }


    proc ::exec {args} {
        variable ::iw::exitCode

        send_log [::iw::ts]
        send_log -- $args
        catch { uplevel iw::tcl_exec $args } results options
        if { [dict get $options -code] eq 0 } { send_log -- "\n$results\n" } { send_log -- "\nFAIL: $args\n" }

        set ::iw::exitCode [dict get $options -code]

        return -options $options -level 0 $results
    }


    # Returns exit code of the last command passed to exec
    #
    # @return integer the exit code
    proc ::$? {} {
        return $::iw::exitCode
    }


    # Returns current timestamp. If called from within another procedure,
    # direct caller name is appended.
    proc ts {} {
        # puts [info level]
        set time \n[clock format [clock seconds] -timezone :UTC -format %+]
        set caller [expr { [info level] > 1 } ? { " : [lindex [info level -1] 0]" : "" }]

        return "$time$caller\n"
    }


    # Processes command line args and fills dictionary provided with key-value pairs.
    # "--manual --user pepa --url https://contoso.com" results in
    # { manual => "", user => "pepa", url => "https://contoso.com", … }
    #
    # @return dictionary dict variable
    proc getOpts {} {
        variable argsDict

        send_log [ts]

        if { [dict size $argsDict] > 0 } {
            return $argsDict
        } else {
            for { set i 0 } { $i < $::argc } { incr i } {
                set key [string trimleft [lindex $::argv $i] "-"]

                if { [string index [lindex $::argv $i+1] 0] != "-" } {
                    incr i
                    dict set argsDict $key [lindex $::argv $i]
                } else {
                    dict set argsDict $key ""
                }
            }
        }

        return $argsDict
    }


    # Returns specified command line argument’s option
    #   (e.g. --user pepa --url https://contoso.com getOpt(user) -> pepa)
    #
    # @return string with argument
    proc getOptArg {opt} {
        dict get [getOpts] $opt
    }


    # Returns whether argument is given on command line or not
    #
    # @return integer (0 - false, anything else true)
    proc optGiven? {opt} {
        dict exists [getOpts] $opt
    }


    # Checks for presence of command line arguments
    #
    # @return integer (0 - false, anything else true)
    proc emptyOpts? {} {
        return [expr {[dict size [getOpts]] == 0} ? 1 : 0]
    }


    # Loads content of variables from icewarp.conf into _dictionary_.
    # { IWS_INSTALL_DIR => "/opt/icewarp", IWS_LIB_DIR => "lib64", … }
    #
    # @return dictionary with contents of icewarp.conf
    proc config {} {
        send_log [ts]
        variable config

        if { [dict size $config] != 0 } { return $config }

        set iwConf [ try { open "/etc/icewarp/icewarp.conf" r
                     } on ok f { split [read $f] "\n"
                     } on error msg { send_log "\nINFO: Couldn’t open icewarp.conf: $msg\n" 
                     } finally { catch {chan close $f}; catch {unset $f} } ]

        foreach line $iwConf {
            if { [regexp "^#" $line] || [expr [string first "=" $line] < 0] } {
                continue
            }
            lassign [split $line "="] key value
            dict set config $key $value
        }

        return $config
    }


    # Checks for existing installation of IceWarp
    #
    # @return integer (0 - false, anything else true)
    proc iwInstalled? {} {
        file isfile /etc/icewarp/icewarp.conf
    }




}
