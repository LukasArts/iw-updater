package require Expect

namespace eval ::du:: {
    variable distInfo [dict create]
    namespace export get*

    set distInfo [dict create]



    # Get value of ID from /etc/os-release.
    #
    # @return Linux ID (e.g. ubuntu or debian)
    proc getLinuxID {} {
        variable distInfo

        if { [dict exists $distInfo linuxID] } {
            return [dict get $distInfo linuxID]
        }

        # We need to handle CentOS 6 and like separately as they do not provide /etc/os-release
        if { [onCentOS?] } {
            return [dict get [dict set distInfo linuxID centos] linuxID
        }

        try {
                exec awk -F= "/^ID=/ \{gsub(/\"/,\"\",\$2); print \$2 \}" /etc/os-release
            } on ok f {
                return [dict get [dict set distInfo linuxID $f] linuxID]
            } on error msg {
                send_log "\nINFO: Couldn’t open /etc/os-release: $msg\n"
                error $msg
            }
    }


    # Construct string under which Linux distributions are recognized at IceWarp.
    # @see https://linuxbuilds.icewarp.com/
    #
    # @return string identification of a distribution (e.g. DEB7 or UBUNTU1404_x64)
    proc getDistro {} {
        variable distInfo

        if { [dict exists $distInfo distro] } {
            return [dict get $distInfo distro]
        }

        set distro [switch -- [getLinuxID] {
            debian { lindex DEB 0 }
            centos { lindex EL 0 }
            ubuntu { lindex UBUNTU 0 }
            default { error "\"[getLinuxID]\"is unsupported Linux distribution."}
        }]

        append distro [dict get [getVersion] major]
        if { [getLinuxID] eq "ubuntu" } { append distro [dict get [getVersion] minor] }
        if { $::tcl_platform(machine) eq "x86_64" } { append distro [lindex _x64 0] }
        
        return [dict get [dict set distInfo distro $distro] distro]
    }


    proc getRelease {} {
    }


    # Get value of VERSION_ID from /etc/os-release
    #
    # @return dictionary with OS version (e.g. { major => 16, minor => 10 }).
    proc getVersion {} {
    # not using lsb_release because it is not guaranteed to be present
        variable distInfo

        if { [dict exists $distInfo version] } {
            return [dict get $distInfo version]
        }

        set major [exec awk -F= "/^VERSION_ID=/ \{ gsub(/\"/,\"\",\$2); print int(\$2) \}" /etc/os-release]
        set minor [exec awk -F. "/^VERSION_ID=/ \{ gsub(/\"/,\"\",\$2); print \$2 \}" /etc/os-release]

        return [dict get [dict set distInfo version [dict create major $major minor $minor]] version]
    }


    # Detect init system
    #
    # @return string identification of pid 1 executable (e.g. init or systemd)
    proc getInit {} {
        variable distInfo

        if { [dict exists $distInfo init] } {
            return [dict get $distInfo init]
        }

        try {
	    file tail [exec ps -p 1 -o comm=]
        } on ok init {
            return [dict get [dict set distInfo init $init] init]
        } on error msg {
            send_log "\nINFO: Failed to detect init system: $msg\n"
            error $msg
        }
    }


    proc systemd? {} {
        return [string match systemd [getInit]]
    }


    proc sysvinit? {} {
        return [string match init [getInit]]
    }


    proc onCentOS? {} {
        file exists /etc/centos-release
    }


}
